const expect = require('chai').expect;
const request = require('supertest');
const _ = require('lodash');
const mongoose = require('mongoose');
const api = '/api';
const server = require('../server');
let db;

describe('Character', () => {
  before(async () => {
    try {
      await mongoose.connect(
        `mongodb+srv://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@harmoney-server-q4jlk.mongodb.net/${process.env.NODE_ENV}?retryWrites=true&w=majority`,
        {
          useNewUrlParser: true,
          useCreateIndex: true,
          useFindAndModify: false,
          useUnifiedTopology: true,
        }
      );
      db = mongoose.connection;
    } catch (error) {
      console.log(error);
    }
  });

  it('should get all Characters', async () => {
    request(server)
      .get(api + `/character`)
      .expect(200)
      .then((resp) => {
        expect(resp.body.message).equals(true);
      });
  });
});
