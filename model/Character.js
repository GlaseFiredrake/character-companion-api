let mongoose = require('mongoose');

const CharacterSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, 'Please add a name'],
  },
  race: {
    type: String,
    required: [true, 'Please add a race'],
  },
  gender: {
    type: String,
    required: [true, 'Please add a gender'],
  },
  backstory: {
    type: String,
    required: [true, 'Please add a backstory'],
  },
  alignment: {
    type: String,
    required: [true, 'Please add a alignment'],
  },
  strength: {
    type: Number,
    required: [true, 'Please add strength'],
  },
  dexterity: {
    type: Number,
    required: [true, 'Please add dexterity'],
  },
  constitution: {
    type: Number,
    required: [true, 'Please add constitution'],
  },
  wisdom: {
    type: Number,
    required: [true, 'Please add wisdom'],
  },
  charisma: {
    type: Number,
    required: [true, 'Please add charisma'],
  },
  intelligence: {
    type: Number,
    required: [true, 'Please add intelligence'],
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

module.exports = mongoose.model('Character', CharacterSchema);
