const express = require('express');
const dotenv = require('dotenv');
const index = require('./routes/index');
const cors = require('cors');
const connectDB = require('./db/db');
//Loan env vars
dotenv.config({ path: './config/config.env' });

const app = express();

// Body parser
app.use(express.json());

// Connect to the DB
connectDB();

app.get('/', (req, res) => {
  res.send('Welcome to the Character Companion API');
});
// Enable CORS - connect to the api from a different domain
app.use(cors());
// Mount router
app.use('/api', index);

const PORT = process.env.PORT || 5000;



app.listen(
  PORT,
  console.log(`Server running in ${process.env.NODE_ENV} mode on port ${PORT}`)
);

module.exports = app;
