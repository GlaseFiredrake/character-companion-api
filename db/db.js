const mongoose = require('mongoose');

const connectDB = async () => {
  const conn = mongoose.connect(
    `mongodb+srv://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@cluster0.6zs31.mongodb.net/${process.env.NODE_ENV}?retryWrites=true&w=majority`,
    {
      useNewUrlParser: true,
      useCreateIndex: true,
      useFindAndModify: false,
      useUnifiedTopology: true,
    }
  ).then(() => console.log('Successfully Connected to [ ' + db.name + ' ]'))
  .catch((err) =>
    console.log(
      'Unable to Connect to [ ' + db.name + ' ]' + ' on all routes',
      err
    )
  );
let db = mongoose.connection;
};

module.exports = connectDB;

