let express = require('express');
let router = express.Router();
let Character = require('../model/Character');

// Method to get a single Character
router.getCharacter = (req, res) => {
  res.setHeader('Content-Type', 'application/json');
  Character.findOne({ _id: req.params.id })
    .then((promis) => {
      res.status(200).send(promis);
    })
    .catch((err) => {
      //console.log(err);
      res.status(500).json({
        error: err,
      });
    });
};

// Method to get all Characters
router.getCharacters = (req, res) => {
  res.setHeader('Content-Type', 'application/json');
  Character.find({})
    .then((promis) => {
      res.status(200).send({ success: true, promis: promis });
    })
    .catch((err) => {
      res.status(500).json({
        error: err,
      });
    });
};

// Method to update a single Character
router.updateCharacter = (req, res, next) => {
  Character.findByIdAndUpdate(req.params.id, req.body, { new: true }, function (
    err,
    character
  ) {
    if (err)
      return res
        .status(500)
        .send('There was a problem updating the character.');
    res.status(200).json({ success: true, character });
  });
};

// Method to delete Character
router.deleteCharacter = (req, res) => {
  res.setHeader('Content-Type', 'application/json');
  Character.deleteOne({ _id: req.params.id })
    .exec()
    .then((promis) => {
      res.status(200).json({ success: true, promis: promis });
    })
    .catch((err) => {
      res.status(500).json({ success: false, error: err });
    });
};

// Method to Create a Character
router.createCharacter = (req, res) => {
  res.setHeader('Content-Type', 'application/json');
  // checks to see if the character already exists
  Character.find({ name: req.body.name })
    .exec()
    .then(() => { 
        const character = new Character({
          race: req.body.race,
          name: req.body.name,
          gender: req.body.gender,
          backstory: req.body.backstory,
          alignment: req.body.alignment,
          dexterity: req.body.dexterity,
          strength: req.body.strength,
          constitution: req.body.constitution,
          wisdom: req.body.wisdom,
          intelligence: req.body.intelligence,
          charisma: req.body.charisma,
        });
        character.save().catch((error) => {
          res.status(403).json({ success: false, error });
        });
        return res.status(201).json({ success: true, character });
    }).catch((err) => {
      res.status(404).json({ success: false, error: err });
    });;
};


router.testendpoint=(req, res)=>{
  return res.send("this is a dummy endpoint")
}


module.exports = router;
