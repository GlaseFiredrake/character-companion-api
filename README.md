[![pipeline status](https://gitlab.com/GlaseFiredrake/character-companion-api/badges/master/pipeline.svg)](https://GlaseFiredrake/character-companion-api/commits/master)

[![coverage report](https://gitlab.com/GlaseFiredrake/character-companion-api/badges/master/coverage.svg)](https://gitlab.com/GlaseFiredrake/character-companion-api/badges/master/coverage.svg?job=coverage)

## Character Companion Web API.

### Staging App: . . . . https://character-companion-dev.herokuapp.com . . . . . .

### Production app: . . . . https://character-companion-prod.herokuapp.com . . . . . .

### GitLab repo: . . . . https://gitlab.com/GlaseFiredrake/character-companion-api . . . .
