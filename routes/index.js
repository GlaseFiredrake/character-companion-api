const express = require('express');
const router = express.Router();
const characterController = require('../controllers/character');

router.get('/character/:id', characterController.getCharacter);
router.get('/character', characterController.getCharacters);
router.post('/character', characterController.createCharacter);
router.put('/character/:id', characterController.updateCharacter);
router.delete('/character/:id', characterController.deleteCharacter);

router.get('/test', characterController.testendpoint);

module.exports = router;
